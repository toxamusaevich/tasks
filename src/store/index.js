import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth/index'
import employee from './modules/employee/index'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        auth: {
            namespaced: true,
            ...auth
        },
        employee: {
            namespaced: true,
            ...employee
        }
    }
})

export default store