const state = () => ({
    access: ''
})

const getters = {
    access: state => state.access
}

const mutations = {
    setState(state, { key, value }) {
        state[key] = value
    }
}

const actions = {
    async login({ dispatch, commit }, payload) {
        let { access } = await dispatch('serverLoginFunction', {
            claims: payload
        })
        commit('setState', { key: 'access', value: access })
        localStorage.setItem('access', access)
    },
    logout({ commit }) {
        localStorage.removeItem('access')
        commit('setState', { key: 'access', value: '' })
    },
    serverLoginFunction({ claims }) {
        const HMACSHA256 = function(stringToSign) {
            // console.log(stringToSign, secret)
            // console.log("not_implemented")
            return stringToSign
        }

        const header = {
            "alg": "HS256",
            "typ": "JWT",
            "kid": "vpaas-magic-cookie-1fc542a3e4414a44b2611668195e2bfe/4f4910"
        }
        const encodedHeaders = btoa(JSON.stringify(header))

        const encodedPlayload = btoa(JSON.stringify(claims))

        const signature = HMACSHA256(`${encodedHeaders}.${encodedPlayload}`, "mysecret")
        const encodedSignature = btoa(signature)

        const jwt = `${encodedHeaders}.${encodedPlayload}.${encodedSignature}`
        return { access: jwt }
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}