const state = () => ({
    employeeList: [{
            id: 1,
            firstName: "Tohir",
            lastName: "Usenov",
            middleName: "Musa o'g'li",
            birthdate: "14.03.2002",
            nationality: "O'zbek",
            birthPlace: "Chinoz shaxri"
        },
        {
            id: 2,
            firstName: "Jahongir",
            lastName: "Musayev",
            middleName: "Xasan o'g'li",
            birthdate: "15.07.2015",
            nationality: "O'zbek",
            birthPlace: "Toshkent shaxri"
        },
        {
            id: 3,
            firstName: "Xasan",
            lastName: "Usenov",
            middleName: "Musa o'g'li",
            birthdate: "13.12.1990",
            nationality: "O'zbek",
            birthPlace: "Jizzax shaxri"
        },
    ],
    mainLength: 0,
    employeeDetail: {}

})

const getters = {
    getEmployeeList(state) {
        return state.employeeList
    },
    getMainLength(state) {
        return state.mainLength
    },
    getEmployeeDetail(state) {
        return state.employeeDetail
    }
}

const mutations = {
    setEmployeeList(state, payload) {
        state.employeeList.push(payload)
        state.mainLength++
    },
    setMainLength(state, payload) {
        state.mainLength = payload
    },
    removeEmployee(state, id) {
        state.employeeList = state.employeeList.filter(el => el.id != id)
    },
    editEmployeeDetail(state, detail, id) {
        let index = state.employeeList.findIndex((el) => el.id == id);
        if (index == -1) {
            state.employeeList[index] = detail
        }
    },
    getEmployeeById(state, id) {
        let index = state.employeeList.findIndex((el) => el.id == id);
        if (index != -1) {
            state.employeeDetail = state.employeeList[index];
            console.log(state.employeeDetail, 'toxa')
        }
    }
}

export default {
    state,
    getters,
    mutations
}